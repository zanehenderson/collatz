#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

#define WORKER_COUNT 12
#define WORK_INCREMENT 1000000000
std::mutex mutex;

void logRange(const std::string& prefix, const uint64_t from, const uint64_t to) {
    // Mutex ensures threads don't attempt to write to output at the same time
    std::lock_guard<std::mutex> lock(mutex);
    std::cout << prefix << " " << from << " - " << to << std::endl;
}

void workFunc(const uint64_t from, const uint64_t to) {
    logRange("Starting:", from, to);

    // Even numbers are skipped since they will immediately divide and resolve to numbers below themselves
    for (uint64_t start = from; start < to; start += 2) {
        uint64_t current = start;

        /*
        Two optimisations:
            1. Any number that eventually reaches below its starting value is considered solved by a previously checked value.
            2. Every odd number is immediately followed by an even number, so perform an even division immediately during this step
        */
        while (current >= start)
            current = (current & 1) ? ((current * 3) + 1) / 2 : current / 2;
    }

    logRange("Finished:", from, to);
}

int main() {
    uint64_t nextWork = 1;

    std::vector<std::thread> workers;

    // Initial worker population
    for (uint8_t i = 0; i < WORKER_COUNT; ++i) {
        // Starting with 1 causes a loop (1-4-2-1), so start the first worker from 3 instead
        workers.push_back(std::thread(workFunc, (nextWork == 1) ? nextWork + 2 : nextWork, nextWork + WORK_INCREMENT));
        nextWork += WORK_INCREMENT;
    }

    // For every worker that completes its work, have it join the main thread and replace it with a new worker
    while (workers.size()) {
        for (auto w = workers.begin(); w != workers.end(); ++w) {
            if (!w->joinable())
                continue;
            
            w->join();
            workers.erase(w--);

            // Once we reach a completion threshold, we stop spinning new workers
            if (nextWork <= UINT64_MAX - WORK_INCREMENT) {
                workers.push_back(std::thread(workFunc, nextWork, nextWork + WORK_INCREMENT));
                nextWork += WORK_INCREMENT;
            }
        }
    }

    std::cout << "Done." << std::endl;
    return 0;
}
